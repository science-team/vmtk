vmtk (1.4.0+dfsg-1) UNRELEASED; urgency=medium

  * Team upload

  [ Steffen Moeller ]
  * debian/upstream/metadata:
    - newly created
    - list reference to paper and entries registries, amongst with biii
  * debian/upstream/edam: Providing minimal annotation.

  [ Andreas Tille ]
  * New upstream version
  * d/watch:
     - repacksuffix +dfsg
     - version 4
     - simplify expression
  * Point Vcs fields to salsa.debian.org
  * Build-Depends vtk7 packages
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Drop ancient X-Python-Version field (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Update renamed lintian tag names in lintian overrides.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Drew Parsons ]
  * update packages to Python 3 (remove Python 2). Closes: #938795.
  * Build-Depends: libvtkgdcm-dev not libvtkgdcm2-dev

 -- Andreas Tille <tille@debian.org>  Mon, 07 Dec 2020 20:33:21 +0100

vmtk (1.3+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Drop build-depends on openjpeg.  Closes: #826828

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 10 Aug 2016 10:09:20 +0000

vmtk (1.3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Move the packaging to git.
  * Run cme fix dpkg-control.
  * Bump lib package name after soname bump.  Closes: #821411
  * Reinstate some lintian overrides removed in the previous upload.
  * Remove non-distributable Windows DLL.

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 11 May 2016 15:34:10 +0000

vmtk (1.3-2) unstable; urgency=medium

  * Team upload.
  * Fix install of hardcoded stuff (Closes: #821298).
  * Use plain dh calls.
  * Drop cdbs
  * Use install files instead of hacky rules.
  * Enable hardening
  * use wl-asneeded to avoid overlinking.
  * Drop lintian overrides.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sun, 17 Apr 2016 10:28:43 +0200

vmtk (1.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release (Closes: #801820)
  * Bump std-version and compat level to 3.9.8 and 9.
  * Build against itk4 and vtk6
  * Fix watch file.
  * Add libfftw3-dev dependency
  * Fix copyright file

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 16 Apr 2016 23:09:46 +0200

vmtk (1.0.1-3) unstable; urgency=low

  * debian/control:
    - Remove DM-Upload-Allowed field.
    - Remove libgdcm2-dev from Build-Depends (closes: #731920).
    - Use canonical URIs for Vcs-* fields.
    - Bump Standards-Version to 3.9.4 (no changes needed).
    - Replace python-support with dh-python.
    - Bump minimum required cdbs package version to 0.4.90~.
    - Bump minimum required python-dev package version to 2.6.6-3~.
    - Remove all XB-Python-Version lines.
    - Replace XS-Python-Version with X-Python-Version.
  * debian/rules: Replace call on dh_pysupport with dh_python2.

 -- Johannes Ring <johannr@simula.no>  Wed, 11 Dec 2013 11:52:08 +0100

vmtk (1.0.1-2) unstable; urgency=low

  * Build tetgen with -O1 instead of -O0 to work around a build failure
    on armhf. See LP: #1049614.
  * debian/control:
    - Use unversioned libtiff-dev instead of libtiff4-dev (closes: #682714).

 -- Johannes Ring <johannr@simula.no>  Thu, 01 Aug 2013 10:15:39 +0200

vmtk (1.0.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Replace libvmtk0.9 with libvmtk1.0.
  * debian/rules:
    - Update VTK_DIR to vtk 5.8 (closes: #676596).
    - Turn on USE_SYSTEM_{VTK,ITK} since default is now to download these.
    - Rename install/libvmtk0.9 target to install/libvtmk1.0.
  * Remove patch (fixed upstream).
  * Update lintian overrides.
  * Switch to machine-readable format in debian/copyright.

 -- Johannes Ring <johannr@simula.no>  Fri, 08 Jun 2012 08:07:49 +0200

vmtk (0.9.0-7) unstable; urgency=low

  * debian/control:
    - Replace libpng12-dev with libpng-dev in Build-Depends (closes: #662541).
    - Add libvtkgdcm2-dev, libvtk5-qt4-dev and libvtk-java in Build-Depends.
    - Bump Standards-Versions to 3.9.3 (no changes needed).
  * debian/rules:
    - Update GDCM_DIR to /usr/lib/gdcm-2.2.

 -- Johannes Ring <johannr@simula.no>  Wed, 21 Mar 2012 13:15:36 +0100

vmtk (0.9.0-6) unstable; urgency=low

  * Add patch to fix problem with Python.h not found (closes: 650586).

 -- Johannes Ring <johannr@simula.no>  Mon, 19 Dec 2011 11:18:55 +0100

vmtk (0.9.0-5) unstable; urgency=low

  * debian/rules: Enable contrib scripts.
  * Add more lintian overrides.

 -- Johannes Ring <johannr@simula.no>  Mon, 05 Sep 2011 13:34:30 +0200

vmtk (0.9.0-4) unstable; urgency=low

  * Make package autobuildable:
    - Set "XS-Autobuild: yes" in debian/control.
    - Add note in debian/copyright.
  * debian/control:
    - Replace libjpeg62-dev with libjpeg-dev in Build-Depends.
    - Bump Standards-Version to 3.9.2 (no changes needed).

 -- Johannes Ring <johannr@simula.no>  Tue, 19 Jul 2011 15:27:19 +0200

vmtk (0.9.0-3) unstable; urgency=low

  * Update/rebuild to use vtk 5.6 (closes: 621070):
    - Set VTK_DIR to /usr/lib/vtk-5.6 in DEB_CMAKE_EXTRA_FLAGS in
      debian/rules.
  * debian/control:
    - Remove old Conflicts, Replaces, and Provides fields for all binary
      packages.
    - Bump Standards-Version to 3.9.1 (no changes needed).

 -- Johannes Ring <johannr@simula.no>  Wed, 06 Apr 2011 20:31:24 +0200

vmtk (0.9.0-2) unstable; urgency=low

  * debian/rules:
    - Enable building of contrib classes. Update lintian-overrides
      accordingly.
    - Minor fix for Python 2.7 for binary package python-vmtk.
  * debian/control: Set XS-Python-Version to ">= 2.3" (closes: #606857).
  * debian/copyright: Remove reference to deprecated BSD license file
    and add full license text.

 -- Johannes Ring <johannr@simula.no>  Mon, 14 Feb 2011 13:54:35 +0100

vmtk (0.9.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Add DM-Upload-Allowed: yes.
    - Bump XS-Python-Version field to 2.6.
  * debian/rules: Use uscan in get-orig-source target.

 -- Johannes Ring <johannr@simula.no>  Fri, 06 Aug 2010 14:34:09 +0200

vmtk (0.9~bzr276-1) unstable; urgency=low

  * Initial release (Closes: #584222)

 -- Johannes Ring <johannr@simula.no>  Thu, 03 Jun 2010 19:11:14 +0200
